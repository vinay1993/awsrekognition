import requests,json

url = "https://rekognition.ap-south-1.amazonaws.com/?Action=DetectLabels"

payload = {"Image":
             {"S3Object": 
                {
                    "Bucket":"www.entrybook.ai",
                    "Name":"laptop2.jpg"}
                }
            }
headers = {
  'Content-Type': 'application/x-amz-json-1.1',
  'X-Amz-Target': 'RekognitionService.DetectLabels',
  'X-Amz-Content-Sha256': 'beaead3198f7da1e70d03ab969765e0821b24fc913697e929e726aeaebf0eba3',
  'X-Amz-Date': '20210702T173910Z',
  'Authorization': 'AWS4-HMAC-SHA256 Credential=AKIAYNND3A47F75LZNPP/20210702/ap-south-1/rekognition/aws4_request, SignedHeaders=content-type;host;x-amz-content-sha256;x-amz-date;x-amz-target,Signature=2bb261b66817ab6feaa7caaa5a68b06052d77adeaed394cfd9c974dd566321a9'
}

response = requests.request("POST", url, headers=headers, data=json.dumps(payload))

print(response.text)